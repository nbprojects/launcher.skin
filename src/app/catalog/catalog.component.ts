import {
  Component,
  OnInit
} from '@angular/core';


@Component({
  selector: 'tx-catalog',  
  styleUrls: [ './catalog.component.scss' ],
  templateUrl: './catalog.component.html'

})
export class CatalogComponent implements OnInit{

    public localState: any;

    private titleDetail: string;
    private displayMyGames: boolean = true;
    private dataGames: any = [];
    private progressValue: number;

   constructor(

   ){
       this.titleDetail = "TEST_TITLE";
       this.progressValue = 0;
       this.random();
   }

   random(){
       setInterval(function(){
            this.progressValue += 1;           

            if(this.progressValue==100){
                this.progressValue = 0;
            }
        }.bind(this),1000);
   }

   ngOnInit(){
        this.loadGames();
   }

   showMyGames(){
       this.displayMyGames = true;
   }

   showAllGames(){
       this.displayMyGames = false;
   }

   hover(event){
       console.log('hover', event);       
   }
   leave(event){
       console.log('leave', event);
   }

   private loadGames() {
    // async load mock data with 'es6-promise-loader'       
    setTimeout(() => {

      System.import('../../assets/mock-data/games-data.json')
        .then((json) => {
          console.log('async mockGamesData', json);
          this.dataGames = json;
        });

    });
  }
}
